<?php defined('SYSPATH') or die('No direct script access');

class Controller_Authenticated extends Controller_App 
{
	public function before()
	{
		parent::before();
		
		if ( ! $this->twitter->is_authenticated())
		{
			$this->request->redirect('');
		}
	}
}