<?php defined('SYSPATH') or die('No direct script access');

class Controller_Session extends Controller_App
{
	public function action_index()
	{
		if ($this->twitter->is_authenticated())
		{
			$this->request->redirect('lists');
		}
		
		$this->response->body(Kotwig_View::factory('logged_out'));
	}
	
	public function action_connect()
	{
		$host = (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) 
			? $_SERVER['HTTP_X_FORWARDED_HOST'] 
			: $_SERVER['HTTP_HOST'];

		$url = $this->twitter->get_authenticate_url('http://'.$host.'/session/callback');
				
		Session::instance()->set('request_secret',
			$this->twitter->get_config('request_secret')
		);

		$this->request->redirect($url);
	}
	
	public function action_callback()
	{
		$this->twitter->authenticate($_GET['oauth_token']);
		
		$session = Session::instance();
		
		$session->set('access_key',
			$this->twitter->get_config('access_key')
		);
		$session->set('access_secret',
			$this->twitter->get_config('access_secret')
		);
		$session->delete('request_secret');

		$session->regenerate();

		$session->set('user_id', $this->twitter->get('account/verify_credentials')->id);

		$this->request->redirect('/');
	}
	
	public function action_logout()
	{
		Session::instance()->destroy();
		
		$this->request->redirect('/');
	}
}