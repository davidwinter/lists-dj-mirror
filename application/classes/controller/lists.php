<?php defined('SYSPATH') or die('No direct script access');

class Controller_Lists extends Controller_Authenticated
{
	public function action_index()
	{
		echo 'lists';
	}
	
	public function action_scrape()
	{
		echo 'scrape';
	}
	
	public function action_doscrape()
	{
		echo 'do scrape';
	}
	
	public function action_save()
	{
		echo 'save';
	}
	
	public function action_createlist()
	{
		echo 'create list';
	}
	
	public function action_removelist()
	{
		echo 'remove list';
	}
}