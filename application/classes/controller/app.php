<?php defined('SYSPATH') or die('No direct script access');

class Controller_App extends Controller
{
	protected $twitter;
	
	public function before()
	{
		$session = Session::instance();
		
		$twitter_options = Kohana::config('listsdj.twitter');
		
		$options = array(
			'consumer_key' => $twitter_options['consumer_key'],
			'consumer_secret' => $twitter_options['consumer_secret'],
			'debug' => true
		);

		$tokens = array('request_secret', 'access_key', 'access_secret');

		foreach ($tokens as $token)
		{
			if ($session->get($token))
			{
				$options[$token] = $session->get($token);
			}
		}

		$this->twitter = new Twitteryauth($options);
	}
}