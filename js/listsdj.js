var ListsDJ = function(list_data) {
	var self;
	
	this.list_data = list_data;
	
	this.save_button = $('#save');
	this.delete_button = $('#delete');
	this.lists_select = $('#lists');
	
	this.friends = $('#friends li');
	
	this.existing = $('#existing li');
	this.to_add = $('#to_add li');
	this.to_remove = $('#to_remove li');
	
	this.create_list_toggle = $('#create_list > a');
	
	this.message_box = $('#message');
}

ListsDJ.MESSAGE_TIMEOUT = 3000;

/**
 * init
 */
ListsDJ.prototype.init = function() {
	self = this;
	this.save_button.click(this.action_save_lists);
	this.delete_button.click(this.action_remove_list);
	this.lists_select.change(this.action_list_changed);
	
	this.friends.click(this.action_add_friend_to_list);
	
	this.existing.live('click', this.action_remove_friend_from_list);
	this.to_add.live('click', this.action_cancel_add_friend_to_list);
	this.to_remove.live('click', this.action_cancel_remove_friend_from_list);
	
	this.create_list_toggle.click(this.action_toggle_create_list);
	
	$('#create_list > div > a').click(this.action_toggle_create_list);
	$('#create_new_list').click(this.action_create_list);
}



/**
 * Action methods
 */
ListsDJ.prototype.action_remove_list = function() {
	console.log('Action: remove list');
	
	if (confirm('Are you sure you wish to delete this list?')) {
		self.disable_save_button();
		$.post('/remove-list', $('#lists').serialize(), function(data) {
			console.log('Ajax: remove list success');
			
			if (data) {
				self.enable_save_button();
				$('#lists option[value="'+data+'"]').remove();
				delete self.list_data[data];
				self.reflect_remove_list();
				
				self.set_message('That list is no more!');
			}
		}, 'json').error(function() {
			console.log('Ajax: remove list error');
			self.enable_save_button();
			self.set_message('Error! That list didn\'t want to be deleted. Stubborn little list.', true);
		});
	}
	
	return false;
}

ListsDJ.prototype.action_create_list = function() {
	console.log('Action: create list');
	
	self.disable_save_button();
	$.post('/create-list', $('#create_list_details > input').serialize(), function(data) {
		console.log ('Ajax: create list success');

		if (data) {
			self.enable_save_button();
			new_list_option = '<option value="'+data.id+'">'+data.name+'</option>';
			self.lists_select.append(new_list_option);
			self.list_data[data.id] = {id: data.id, name: data.name, member_ids: []};
			self.action_toggle_create_list();
			$('#create_list_details > input').val('');
			
			self.set_message('Woo! You got yourself a new list. Check it out!');
		}
	}, 'json').error(function() {
		console.log('Ajax: create list error');
		self.enable_save_button();
		self.set_message('Error! We had some trouble getting that list created. Sorry.', true);
	});
	
	return false;
}

ListsDJ.prototype.action_toggle_create_list = function() {
	console.log('Action: toggle create list');
	
	div = $('#create_list div');
	
	if (div.is(':visible')) {
		div.hide();
	}
	else {
		div.css('display', 'inline');
	}
	
	return false;
}

ListsDJ.prototype.action_save_lists = function() {
	console.log('Action: save lists');
	
	self.disable_save_button();
	
	$.post('/save', $('#submitted_changes select, #submitted_changes input').serialize(), function(data) {
		console.log('Ajax: success');

		if (data) {
			self.enable_save_button();
			
			self.reflect_save_changes();
			
			self.set_message('Changes to the list all done.');
		}
	}, 'json').error(function() {
		self.set_message('Error! Sorry, those changes couldn\'t be made.', true);
		self.enable_save_button();
	});
	
	return false;
}

ListsDJ.prototype.action_list_changed = function() {
	console.log('Action: list changed');
	
	if (self.is_unsaved_changes()) {
		if (!confirm('There are unsaved changes. Are you sure?')) {
			/**
			 * Abort change, and set to the list being worked on
			 */
			self.lists_select.val(
				self.lists_select.data('old_choice')
			);
			
			return false;
		}
	}
	
	/**
	 * Clear out all lists in header ready for next list.
	 */
	$('#members ul').empty();
	
	list_id = self.get_list_selection();
	
	if (list_id) {
		/**
		 * Ensure that the existing list is emptied, sometimes it doesn't clear.
		 */		
		$('#existing ul').empty();
		
		/**
		 * A valid list has been selected, so we want to enable the save button.
		 *
		 * @todo only enable the button if there are items in the to_add or to_remove lists
		 */
		self.enable_save_button();
		
		self.show_hidden_friends();
		
		self.update_existing_members_for_list(list_id);
	}
	else {
		self.disable_save_button();
		self.show_hidden_friends();
	}
	
	/**
	 * Save current list choice in case we need to revert back.
	 */
	self.lists_select.data('old_choice', self.lists_select.val());
}

ListsDJ.prototype.action_add_friend_to_list = function() {
	console.log('Action: add friend to list');
	
	if (self.is_save_in_progress()) {
		return false;
	}
	
	if (self.is_valid_list_selection()) {
		friend_id = self.get_friend_id_from_hidden($(this));
		
		self.hide_friend_div(friend_id);
		
		self.clone_friend_into(friend_id,'#to_add ul')
			.find('input').attr('name', 'to_add[]');
	}

	return false;
}

ListsDJ.prototype.action_remove_friend_from_list = function() {
	console.log('Action: remove friend from list');
	
	if (self.is_save_in_progress()) {
		return false;
	}
	
	$(this).remove().prependTo('#to_remove ul').find('input').attr('name', 'to_remove[]');
}

ListsDJ.prototype.action_cancel_add_friend_to_list = function() {
	console.log('Action: cancel add friend to list');
	
	if (self.is_save_in_progress()) {
		return false;
	}
	
	$(this).remove();
	
	self.show_friend(
		self.get_friend_id_from_hidden($(this))
	);
}

ListsDJ.prototype.action_cancel_remove_friend_from_list = function() {
	console.log('Action: cancel remove friend from list');
	
	if (self.is_save_in_progress()) {
		return false;
	}
	
	$(this).remove().prependTo('#existing ul').find('input').removeAttr('name');
}



/**
 * Helper methods
 */
ListsDJ.prototype.is_save_in_progress = function() {
	return (self.save_button.attr('disabled') == 'disabled')
}

ListsDJ.prototype.reflect_save_changes = function() {
	$('#to_remove li').each(function () {
		friend_id = self.get_friend_id_from_hidden($(this));
		self.remove_friend_from_list(self.get_list_selection(), friend_id);
		
		$(this).remove();
		
		self.show_friend(friend_id);
	});
					
	$('#to_add li').each(function() {
		friend_id = self.get_friend_id_from_hidden($(this));
		self.add_friend_to_list(self.get_list_selection(), friend_id);

		$(this).remove().prependTo('#existing ul')
			.find('input').removeAttr('name');
	});
}

ListsDJ.prototype.reflect_remove_list = function() {
	$('#lists').val('');
	$('#lists').change();
}

ListsDJ.prototype.enable_save_button = function() {
	self.save_button.removeAttr('disabled');
	self.delete_button.removeAttr('disabled');
}

ListsDJ.prototype.disable_save_button = function() {
	self.save_button.attr('disabled', 'disabled');
	self.delete_button.attr('disabled', 'disabled');
}

ListsDJ.prototype.get_list_selection = function() {
	return self.is_valid_list_selection();
}

ListsDJ.prototype.add_friend_to_list = function(list_id, friend_id) {
	self.list_data[list_id]['member_ids'].push(parseInt(friend_id));
}

ListsDJ.prototype.remove_friend_from_list = function(list_id, friend_id) {
	index = self.list_data[list_id]['member_ids'].indexOf(parseInt(friend_id));
	delete self.list_data[list_id]['member_ids'][index];
}

ListsDJ.prototype.is_valid_list_selection = function() {
	return self.lists_select.val();
}

ListsDJ.prototype.show_friend = function(friend_id) {
	$('#friend_'+friend_id).children().fadeIn(function() {
		$(this).parent().removeClass('hide');
	});
}

ListsDJ.prototype.get_friend_id_from_hidden = function(item) {
	return $('input', item).val();
}

ListsDJ.prototype.is_unsaved_changes = function() {
	return ($('#to_add li').size() > 0 || $('#to_remove li').size() > 0);
}

ListsDJ.prototype.show_hidden_friends = function() {
	$('#friends li.hide > div').fadeIn(function() {
		$(this).parent().removeClass('hide');
	});
}

ListsDJ.prototype.get_member_ids_for_list = function(list_id) {
	return list_data[list_id]['member_ids'];
}

ListsDJ.prototype.hide_friend_div = function(friend_id) {
	friend_div = $('#friend_'+friend_id).addClass('hide');
	friend_div.children().fadeOut(function() {
		
	});
}

ListsDJ.prototype.update_existing_members_for_list = function(list_id) {
	members = self.get_member_ids_for_list(list_id); 
		
	for (i in members) {
		self.hide_friend_div(members[i]);
		self.clone_friend_into(members[i], '#existing ul');
	}
}

ListsDJ.prototype.clone_friend_into = function(friend_id, selector) {
	clone = $('#friend_'+friend_id).clone();
	clone.children().hide();
	clone.removeAttr('id').prependTo(selector);
	
	clone.children().fadeIn(function() {
		$(this).removeAttr('style');
		$(this).parent().removeClass('hide');
	});
	
	return clone;
}

ListsDJ.prototype.set_message = function(text, error) {
	error = error || false;
	
	if (error) {
		self.message_box.addClass('error');
	}
	
	self.message_box.html('<p>'+text+'</p>').slideDown(function() {
		setTimeout(function() {
			$('#message').slideUp(function() {
				$(this).removeClass('error');
			});
		}, ListsDJ.MESSAGE_TIMEOUT)
	});
}